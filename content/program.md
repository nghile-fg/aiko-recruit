---json
{
    "title": "サマー・クラーク・プログラム",
    "description": "弁護士法人あい湖法律事務所の採用情報サイト。東京・滋賀・大阪を中心に活躍していただける弁護士・事務職員を募集中。専門性が高く優れた法律サービスを提供しています。ロースクールに在籍者を対象にサマー・クラーク・プログラムも実施しております。",
    "keywords":"法律,事務所,弁護士,事務員,採用,あい湖,東京,滋賀,大阪,サマークラーク,新卒,中途,リクルート", 
    "slug": "",
    "layout": "program.html",
    "tag": [],
    "date": "2017-04-18T04:38:58.000Z",
    "publishDate": "2017-04-18T04:38:58.000Z",
    "draft": false,
    "__content__": ""
}
---
