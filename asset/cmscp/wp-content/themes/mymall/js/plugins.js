var href = window.location.pathname.split('/');
var n_href = href.splice(-1)[0];
if (n_href == '/' || n_href == '/index.html' || n_href == '' || n_href == 'index.html') {
    $('#main').removeClass('main-page');
}else {
    $('#main').addClass('main-page');
}
$(".home-03-video-slider").slick({
    autoplaySpeed:4000,
    lazyLoad:"progressive",
    speed:600,
    arrows:true,
    prevArrow: '<span class="home-03-left"></span>',
    nextArrow: '<span class="home-03-right"></span>',
    dots:false,
    cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)"
});
